#ifndef READFILE_H
#define READFILE_H
#include<QFile>
#include<QTextStream>
class ReadFile
{
public:
    ReadFile();
    void openFile(const QString fileName);
    void closeFile();
    void processFile();
private:
    int row;
    int colom;
};

#endif // READFILE_H
