#include "charts.h"
#include<QtWidgets>
#include <QtCore/QDebug>
#include <QtCore/QtMath>

Charts::Charts(QWidget*parent):QWidget(parent)
{
    QGridLayout *layout=new QGridLayout;
    layout->addWidget(view());
    setLayout(layout);

}
QChartView *Charts::view()
{
    QBarSet *set0 = new QBarSet("Messi");
    QBarSet *set1 = new QBarSet("Ronaldo");
    QBarSet *set2 = new QBarSet("seng hong");
    QBarSet *set3 = new QBarSet("hak");
    QBarSet *set4 = new QBarSet("sophal");
    QBarSet *set5 = new QBarSet("Sen");


    *set0 << 1 << 2 << 3 << 4 << 5 << 6;
    *set1 << 5 << 0 << 0 << 4 << 0 << 7;
    *set2 << 3 << 5 << 8 << 13 << 8 << 5;
    *set3 << 5 << 6 << 7 << 3 << 4 << 5;
    *set4 << 9 << 7 << 5 << 3 << 1 << 1;
    *set5 << 9 << 7 << 20 << 3 << 1 << 1;
//![1]
//![2]
    QBarSeries *series = new QBarSeries();
    series->append(set0);
    series->append(set1);
    series->append(set2);
    series->append(set3);
    series->append(set4);
    series->append(set5);

//![2]

//![3]
    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Simple barchart example");
    chart->setAnimationOptions(QChart::SeriesAnimations);
//![3]

//![4]
    QStringList categories;
    categories << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun";
    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(categories);
    chart->createDefaultAxes();
    chart->setAxisX(axis, series);
//![4]

//![5]
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
//![5]

//![6]
    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->chart()->setTheme(QChart::ChartThemeBlueCerulean);
    return chartView;
}

