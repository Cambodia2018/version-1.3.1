#ifndef CHARTS_H
#define CHARTS_H
#include <Qtcharts>
#include <QWidget>

class Charts:public QWidget
{
    Q_OBJECT
public:
    explicit Charts(QWidget*parent=nullptr);
private:
    QChartView *view();
};

#endif // CHARTS_H
