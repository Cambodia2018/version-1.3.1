#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QFile>
#include<QFileDialog>
#include"Plotting/graphic.h"
#include<Simulation/simulation.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowState(Qt::WindowMaximized);
    customize();
    connect(ui->button5,&QPushButton::clicked,this,&MainWindow::broswer);
}
void MainWindow::graphic()
{
    Graphic *window=new Graphic();
    window->show();
}
void MainWindow::simulation()
{
    Simulation *window=new Simulation();
    window->show();
}

void MainWindow::broswer()
{
    const QString fileName = QFileDialog::getOpenFileName(this,
    "Open file",QDir::rootPath(), "excel Files (*.xlsx *.xls *.csv)");
    if (!fileName.isEmpty())
        openFile(fileName);
}
void MainWindow::openFile(const QString &fileName)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
}
MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::customize()
{
    QPixmap pix(":/Icon/error.png");
    QIcon icon(pix);
    ui->button4->setIcon(icon);
    ui->button4->setIconSize(pix.size());
    QPixmap pix1(":/Icon/aim.png");
    QIcon icon1(pix1);
    ui->button1->setIcon(icon1);
    ui->button1->setIconSize(pix1.size());
    QPixmap pix2(":/Icon/plot.png");
    QIcon icon2(pix2);
    ui->button2->setIcon(icon2);
    ui->button2->setIconSize(pix2.size());
}

