#include "widget.h"
#include<cmath>
Widget::Widget(QWidget*parent):QWidget(parent){
  setFixedSize(1200, 768);
  background=QBrush(QColor("#696969"));
  color1=QBrush(QColor("#FF1493"));
  color2=QBrush(QColor("#0000FF"));
  color3=QBrush(QColor("#7CFC00"));
  color4=QBrush(QColor("#2F4F4F"));
  car=new Car(this);
}
void Widget::paintEvent(QPaintEvent *event){
    QPainter painter;
    painter.begin(this);
    painter.fillRect(event->rect(),background);
    car->paintEvent(&painter,event);
    painter.end();
}
void Widget::animate(){
    update();
}
void Widget::setValue(){}
