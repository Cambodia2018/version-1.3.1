#ifndef SIMULATION_H
#define SIMULATION_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QTimer>
#include"widget.h"
#include<QPushButton>
#include<QLineEdit>
class Simulation :public QWidget
{
public:
    Simulation();
    ~Simulation();
private slots:
    void startAnimation();
    void stopAnimation();
    Widget*native;
    QTimer* timer;
    QPushButton *button;
    QPushButton *button2;
    QBrush background;

};
#endif // SIMULATION_H
