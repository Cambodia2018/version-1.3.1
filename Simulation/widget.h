#ifndef WIDGET_H
#define WIDGET_H
#include <QWidget>
#include <QPainter>
#include <QTimer>
#include<QBrush>
#include <QPaintEvent>
#include"car.h"

class Widget :public QWidget
{
public:
    Widget(QWidget *parent);
protected:
    void paintEvent(QPaintEvent*event) override;
public slots:
    void animate();
    void setValue();
private:
    QBrush background;
    QBrush color1;
    QBrush color2;
    QBrush color3;
    QBrush color4;
    Car*car;


};

#endif // WIDGET_H
