#-------------------------------------------------
#
# Project created by QtCreator 2018-02-27T12:40:26
#
#-------------------------------------------------

QT+= core gui
QT       += core gui
QT += charts
QT += axcontainer

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Application
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    Simulation/car.cpp \
    Simulation/simulation.cpp \
    Simulation/widget.cpp \
    Visualizaion/charts.cpp \
    plotting/graphic.cpp \
    plotting/ploting.cpp

HEADERS += \
        mainwindow.h \
    Simulation/car.h \
    Simulation/simulation.cpp.autosave \
    Simulation/simulation.h \
    Simulation/simulation.h.autosave \
    Simulation/widget.h \
    Visualizaion/charts.h \
    plotting/graphic.h \
    plotting/ploting.h

FORMS += \
        mainwindow.ui \
    plotting/graphic.ui

DISTFILES +=

RESOURCES += \
    icon.qrc
