#ifndef GRAPHIC_H
#define GRAPHIC_H
#include <QMainWindow>
#include<QtGui>

namespace Ui {
class Graphic;
}

class Graphic : public QMainWindow
{
    Q_OBJECT

public:
    explicit Graphic(QWidget *parent = 0);
    ~Graphic();

private slots:

    void on_button_clicked();

private:
    Ui::Graphic *ui;
};

#endif // GRAPHIC_H
