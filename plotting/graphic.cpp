#include "graphic.h"
#include "ui_graphic.h"

Graphic::Graphic(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Graphic)
{
    ui->setupUi(this);
    setWindowState(Qt::WindowMaximized);
    setWindowTitle("Ploting");

}
Graphic::~Graphic()
{
    delete ui;
}

void Graphic::on_button_clicked(){

    QString text=ui->lineEdit->text();
    ui->widget->setValue(text);
    ui->widget->update();
}
