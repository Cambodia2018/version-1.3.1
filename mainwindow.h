#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include<QtGui>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void customize();
public slots:
    void graphic();
    void simulation();
    void broswer();

private slots:

private:
    void openFile(const QString &fileName);
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
